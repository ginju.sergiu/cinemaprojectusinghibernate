package com.sda.serghei.crud.repository;

import com.sda.serghei.crud.model.Client;
import com.sda.serghei.crud.utils.SessionManager;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ClientRepository {
    public void create(Client client){
        Transaction transaction = null;
        try{
            Session session = SessionManager.getSessionFactory().openSession();
           transaction = session.beginTransaction();
            session.save(client);
            transaction.commit();
            session.close();

        }catch(Exception e){
            e.printStackTrace();
            if(transaction != null){
                transaction.rollback();
            }
        }
    }
    public List<Client> findAll(){
        try{
            Session session = SessionManager.getSessionFactory().openSession();
          // List<Client>clients =  session.createQuery("from Client ",Client.class).list();
           List<Client>clients =  session.createQuery("select c from Client c",Client.class).list();
           session.close();
           return clients;

        }catch(Exception e){
            e.printStackTrace();
        }
        return new ArrayList<>();

    }
}
