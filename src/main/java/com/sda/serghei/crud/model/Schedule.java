package com.sda.serghei.crud.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "schedule")
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "scedhule_id")
    private Integer id;
    @Column(name = "start_time")
    private Date startTime;

    @ManyToOne
    @JoinColumn(name= "movie_id")
    private Movie movie;

    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room room;

    // maybe for admin role
   /* @OneToMany(mappedBy = "scdhedule")
    private Set<Reservation> reservations;*/

    // maybe for admin role
   /* @OneToMany(mappedBy = "schedule")
    private Set<Ticket>tickets;*/


    public Schedule() {
    }

    public Schedule(Date startTime, Movie movie, Room room) {
        this.startTime = startTime;
        this.movie = movie;
        this.room = room;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", startTime=" + startTime +
                ", movie=" + movie +
                ", room=" + room +
                '}';
    }
}
