package com.sda.serghei.crud.model;

import javax.persistence.*;

@Entity
@Table(name = "seat")
public class Seat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "seat_id")
    private Integer id;
    @Column(name = "seat_row")
    private Integer row;
    @Column(name = "number")
    private Integer number;


    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room room;

    /*@OneToMany(mappedBy = "seat")
    private Set<ReservationSeat> reservationSeats; */

    // maybe for admin role
    /*@OneToMany(mappedBy = "seat")
    private Set<Ticket>tickets;*/

    public Seat() {
    }

    public Seat(Integer row, Integer number, Room room) {
        this.row = row;
        this.number = number;
        this.room = room;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }


    @Override
    public String toString() {
        return "Seat{" +
                "id=" + id +
                ", row=" + row +
                ", number=" + number +
                ", room=" + room +
                '}';
    }
}
