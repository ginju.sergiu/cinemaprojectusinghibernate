package com.sda.serghei.crud.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ticket_category")
public class TicketCategory {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name = "ticket_category_id")
    private Integer id;
    @Column(name = "type")
    private String type;
    @Column(name = "price")
    private Integer price;

    /*@OneToMany(mappedBy = "ticket_category")
    private Set<Ticket> tickets; */

    public TicketCategory() {
    }

    public TicketCategory(String type, Integer price) {
        this.type = type;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "TicketCategory{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }
}
