package com.sda.serghei.crud.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "room")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "room_id")
    private Integer id;
    @Column(name = "number")
    private Integer number;
    @Column(name = "max_seats")
    private Integer maxSeats;
    @Column(name = "location")
    private String location;

   /* @OneToMany(mappedBy = "room")
    private Set<Schedule> schedules;*/

    @OneToMany(mappedBy = "room")
    Set<Seat> seats;

    public Room() {
    }

    public Room(Integer number, Integer maxSeats, String location) {
        this.number = number;
        this.maxSeats = maxSeats;
        this.location = location;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(Integer maxSeats) {
        this.maxSeats = maxSeats;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Set<Seat> getSeats() {
        return seats;
    }

    public void setSeats(Set<Seat> seats) {
        this.seats = seats;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", number=" + number +
                ", maxSeats=" + maxSeats +
                ", location='" + location + '\'' +
                '}';
    }
}