package com.sda.serghei.crud.utils;

import com.sda.nedelcu.crud.model.*;
import com.sda.serghei.crud.model.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionManager extends AbstractSessionManager {
    private static final SessionManager SESSION_MANAGER = new SessionManager();

    @Override
    protected void setAnnotatedClasses(Configuration configuration) {
        configuration.addAnnotatedClass(Client.class);
        configuration.addAnnotatedClass(Movie.class);
        configuration.addAnnotatedClass(TicketCategory.class);
        configuration.addAnnotatedClass(Room.class);
        configuration.addAnnotatedClass(Schedule.class);
        configuration.addAnnotatedClass(Reservation.class);
        configuration.addAnnotatedClass(Seat.class);
        configuration.addAnnotatedClass(ReservationSeat.class);
        configuration.addAnnotatedClass(Ticket.class);




    }

    public static SessionFactory getSessionFactory() {
        return SESSION_MANAGER.getSessionFactory("cinema_app");
    }

    public static void shutDown() {
        SESSION_MANAGER.shutdownSessionManager();
    }

}
