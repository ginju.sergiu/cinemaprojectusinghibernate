package com.sda.serghei.crud;

import com.sda.serghei.crud.model.Client;
import com.sda.serghei.crud.repository.ClientRepository;
import com.sda.serghei.crud.utils.SessionManager;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Client client1 = new Client("Ginju","Serghei","ginju@mail.ru",new Date());
        Client client2 = new Client("Ginju","Felicia","ginju94@mail.ru",new Date());
        ClientRepository clientRepository = new ClientRepository();
        clientRepository.create(client1);
        clientRepository.create(client2);
        clientRepository.findAll();


        //last method in main!
        SessionManager.shutDown();
    }
}
